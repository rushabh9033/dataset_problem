// Write a function called doubleValues which accepts an array and returns a new array with all the values in the array passed to the function doubled.
function doubleValues(arr) {
  let doubleVal = arr.map((element) => {
    return element * 2;
  });
  return doubleVal;
}
doubleValues([5, 1, 2, 3, 10]);
doubleValues([1, 2, 3]);

// Write a function called onlyEvenValues which accepts an array and returns a new array with only the even values in the array passed to the function.
function onlyEvenValues(arr) {
  let evenArr = arr.filter((element) => element % 2 == 0);
  return evenArr;
}

onlyEvenValues([1, 2, 3]);
onlyEvenValues([5, 1, 2, 3, 10]);

// Write a function called showFirstAndLast which accepts an array of strings and returns a new array with only the first and last character of each string.
function showFirstAndLast(arr) {
  let firstAndLast = arr.map(
    (element) => element.charAt(0) + element.slice(-1)
  );

  return firstAndLast;
}
showFirstAndLast(["colt", "matt", "tim", "test"]);
showFirstAndLast(["hi", "goodbye", "smile"]);

//Write a function called addKeyAndValue which accepts an array of objects, a key, and a value and returns the array passed to the function with the new key and value added for each object.
function addKeyAndValue(arr, key, value) {
  let newArr = arr.map((element) => {
    element[key] = value;
    return element;
  });
  return newArr;
}

addKeyAndValue(
  [{ name: "Elie" }, { name: "Tim" }, { name: "Matt" }, { name: "Colt" }],
  "title",
  "instructor"
);

// Write a function called vowelCount which accepts a string and returns an object with the keys as the vowel and the values as the number of times the vowel appears in the string. This function should be case insensitive so a lowercase letter and uppercase letter should count.

function vowelCount(str) {
  let obj = {};
  str.split("").forEach((element) => {
    const vowel = "AEIOU";
    element = element.toUpperCase();
    if (vowel.includes(element)) {
      if (obj[element]) {
        obj[element] += 1;
      } else {
        obj[element] = 1;
      }
    }
    return element;
  });
  return obj;
}

vowelCount("Elie");
vowelCount("Tim");
vowelCount("Matt");
vowelCount("hmmm");
vowelCount("I Am awesome and so are you");

// Write a function called doubleValuesWithMap which accepts an array and returns a new array with all the values in the array passed to the function doubled.

function doubleValuesWithMap(arr) {
  let doubleWithMap = arr.map((element) => {
    return element * 2;
  });
  return doubleWithMap;
}
doubleValuesWithMap([1, 2, 3]);
doubleValuesWithMap([1, -2, -3]);

// Write a function called valTimesIndex which accepts an array and returns a new array with each value multiplied by the index it is currently at in the array.

function valTimesIndex(arr) {
  let mulByIndex = arr.map((element, index) => {
    return element * index;
  });
  return mulByIndex;
}
valTimesIndex([1, 2, 3]);
valTimesIndex([1, -2, -3]);

//Write a function called extractKey which accepts an array of objects and some key and returns a new array with the value of that key in each object.

function extractKey(arr, key) {
  let getVal = arr.map((element) => {
    element = element[key];
    return element;
  });
  return getVal;
}

extractKey(
  [{ name: "Elie" }, { name: "Tim" }, { name: "Matt" }, { name: "Colt" }],
  "name"
);

// Write a function called extractFullName which accepts an array of objects and returns a new array with the value of the key with a name of "first" and the value of a key with the name of  "last" in each object, concatenated together with a space.

function extractFullName(arr) {
  let fullName = arr.map((element) => {
    return element.first + " " + element.last;
  });
  return fullName;
}

extractFullName([
  { first: "Elie", last: "Schoppik" },
  { first: "Tim", last: "Garcia" },
  { first: "Matt", last: "Lane" },
  { first: "Colt", last: "Steele" },
]);

// Write a function called filterByValue which accepts an array of objects and a key and returns a new array with all the objects that contain that key.

function filterByValue(arr, key) {
  let filterValue = arr.filter((element) => key in element);
  return filterValue;
}

filterByValue(
  [
    { first: "Elie", last: "Schoppik" },
    { first: "Tim", last: "Garcia", isCatOwner: true },
    { first: "Matt", last: "Lane" },
    { first: "Colt", last: "Steele", isCatOwner: true },
  ],
  "isCatOwner"
);

// Write a function called find which accepts an array and a value and returns the first element in the array that has the same value as the second parameter or undefined if the value is not found in the array.

function find(arr, searchValue) {
  let findVal = arr.includes(searchValue);
  if (findVal) {
    return searchValue;
  } else {
    return undefined;
  }
}
find([1, 2, 3, 4, 5], 10);
find([1, 2, 3, 4, 5], 3);

// Write a function called findInObj which accepts an array of objects, a key, and some value to search for and returns the first found value in the array.

function findInObj(arr, key, searchValue) {
  let findObj = arr.filter((element) => element[key] == searchValue)[0];
  return findObj;
}

findInObj(
  [
    { first: "Elie", last: "Schoppik" },
    { first: "Tim", last: "Garcia", isCatOwner: true },
    { first: "Matt", last: "Lane" },
    { first: "Colt", last: "Steele", isCatOwner: true },
  ],
  "isCatOwner",
  true
);

// Write a function called removeVowels which accepts a string and returns a new string with all of the vowels (both uppercased and lowercased) removed. Every character in the new string should be lowercased.

function removeVowels(str) {
  let newStr = "";
  str.split("").forEach((element) => {
    let vowel = "aeiou";
    let lowercase = element.toLowerCase();
    if (!vowel.includes(lowercase)) {
      newStr += element;
    }
    return element;
  });
  return newStr;
}
removeVowels("ZZZZZZ");
removeVowels("Elie");
removeVowels("TIM");

// Write a function called doubleOddNumbers which accepts an array and returns a new array with all of the odd numbers doubled (HINT - you can use map and filter to double and then filter the odd numbers).

function doubleOddNumbers(arr) {
  let doubleNumber = arr
    .filter((element) => element % 2 !== 0)
    .map((element) => element * 2);
  return doubleNumber;
}
doubleOddNumbers([1, 2, 3, 4, 5]);
doubleOddNumbers([4, 4, 4, 4, 4, 4]);

// Write a function called `findUserByUsername` which accepts an array of objects, each with a key of username, and a string. The function should return the first object with the key of username that matches the string passed to the function. If the object is not found, return undefined.
const users = [
  { username: "mlewis" },
  { username: "akagen" },
  { username: "msmith" },
];
function findUserByUsername(usersArray, username) {
  let findName = usersArray.find((element) => element.username == username);
  return findName;
}
findUserByUsername(users, "mlewis");
findUserByUsername(users, "taco");

// Write a function called `removeUser` which accepts an array of objects, each with a key of username, and a string. The function should remove the object from the array. If the object is not found, return undefined.

function removeUser(usersArray, username) {
  let removeIndex = usersArray.findIndex(
    (element) => Object.values(element) == username
  );
  let newObj = { ...usersArray[removeIndex] };

  usersArray.splice(newObj);
  return newObj;
}

removeUser(users, "akagen");

// Write a function called extractValue which accepts an array of objects and a key and returns a new array with the value of each object at the key.
const arr = [
  { name: "Elie" },
  { name: "Tim" },
  { name: "Matt" },
  { name: "Colt" },
];
function extractValue(arr, key) {
  let value = arr.reduce((accumlator, currentVal) => {
    accumlator.push(currentVal[key]);
    return accumlator;
  }, []);
  return value;
}
extractValue(arr, "name");

// Write a function called vowelCount which accepts a string and returns an object with the keys as the vowel and the values as the number of times the vowel appears in the string. This function should be case insensitive so a lowercase letter and uppercase letter should count.

function vowelCount(str) {
  let count = str.split("").reduce((accumlator, currentVal) => {
    let vowel = "aeiou";
    currentVal = currentVal.toLowerCase();
    if (vowel.includes(currentVal)) {
      if (accumlator[currentVal]) {
        accumlator[currentVal] += 1;
      } else {
        accumlator[currentVal] = 1;
      }
    }
    return accumlator;
  }, {});
  return count;
}
vowelCount("Elie");
vowelCount("Tim");
vowelCount("hmmm");
vowelCount("I Am awesome and so are you");

// Write a function called addKeyAndValue which accepts an array of objects and returns the array of objects passed to it with each object now including the key and value passed to the function.

function addKeyAndValue(arr, key, value) {
  let keyValue = arr.reduce((accumlator, currentVal) => {
    currentVal[key] = value;
    accumlator.push(currentVal);
    return accumlator;
  }, []);
  return keyValue;
}

addKeyAndValue(arr, "title", "Instructor");

// Write a function called partition which accepts an array and a callback and returns an array with two arrays inside of it. The partition function should run the callback function on each value in the array and if the result of the callback function at that specific value is true, the value should be placed in the first subarray. If the result of the callback function at that specific value is false, the value should be placed in the second subarray.
const arr1 = [1, 2, 3, 4, 5, 6, 7, 8];

let isEven = (array, value) => {
  if (value % 2 == 0) {
    array[0].push(value);
  } else {
    array[1].push(value);
  }
  return array;
};

function partition(array, callback) {
  let partitionArr = array.reduce(callback, [[], []]);
  return partitionArr;
}

partition(arr1, isEven);
